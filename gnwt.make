; A semicolon denotes a comment. Anything the appears after a semicolon on the same line will be ignored by the parser, as will blank lines.

; Drupal 7 standard gnwt site makefile
; To use this file you need to install drush
; Run the command "drush" to check if it is currenty installed
; If it is, skip to the next section, otherwise install drush using these as guides:
; http://docs.drush.org/en/master/install/
; http://docs.drush.org/en/master/install-alternative/

; Next you will have to download the makefile. The simplest way to do that is to use the git repository: https://bitbucket.org/gnwt-cue/gnwt-drupal-make/
; Either clone or download it

; Next you have to run the command drush make gnwt-drupal-make/gnwt.make /your/directory/for/your/site
; You should now have a base fileset of the gnwt drupal standard site
; Any other themes or modules should not be added to this, they should be downloaded seperatly and documented

; MAKEFILE STARTS HERE
;----------------------

; Specify drupal 7 core
core = 7.x

; Always api 2
api = 2

; Set default subdirectory to be /contrib as most themes and modules are contrib ones
defaults[projects][subdir] = contrib

; PROJECTS
; --------
; Download drupal
projects[drupal] = 7.56
projects[drupal][patch][] = "https://www.drupal.org/files/issues/node_access_for_left_joins_d7.1349080-332.patch"
projects[drupal][patch][] = "https://www.drupal.org/files/issues/1803886-32-savepoint_1-does-not-exist.patch"
projects[drupal][patch][] = "https://www.drupal.org/files/issues/use-specified-row-weights-2679983-3.patch"

;THEMES
;------
; Zen theme that is used with gnwt_base_theme
projects[zen][subdir] = ""

;gnwt base theme
projects[gnwt_base_theme][type] = 'theme'
projects[gnwt_base_theme][download][type] = 'git'
projects[gnwt_base_theme][download][url] = "https://bitbucket.org/gnwt-cue/gnwt_base_theme.git"
projects[gnwt_base_theme][subdir] = ""

;MODULES
;-------
projects[addressfield] = 1.2
projects[admin_language] = 1.0-beta4
projects[admin_menu] = 3.0-rc5
projects[admin_views] = 1.6
projects[advanced_help] = 1.3
projects[auto_entitylabel] = 1.4
projects[better_exposed_filters] = 3.4
projects[better_formats] = 1.0-beta2
projects[block_class] = 2.3
projects[book_helper] = 1.0-beta3
projects[book_menus] = 1.x-dev
projects[book_title_override] = 1.2
projects[ccl] = 1.6
projects[chosen] = 2.1
projects[ckeditor] = 1.18
projects[cleanpager] = 1.0
projects[colorbox] = 2.13
projects[conditional_fields] = 3.0-alpha2
projects[content_access] = 1.2-beta2
projects[ctools] = 7.x-1.12
projects[ctools][patch][] = "https://www.drupal.org/files/issues/ctools-2828620-60-View-pane-rendering-misuses-cache-revert-1910608.patch"
projects[context] = 3.7
projects[date] = 2.10
projects[diff] = 3.3
projects[drafty] = 1.0-beta4
projects[draggableviews] = 2.1
projects[ds] = 2.14
projects[email] = 1.3
projects[entity] = 1.8
projects[entityconnect] = 2.0-rc1
projects[entityreference] = 1.4
projects[entity_translation] = 7.x-1.0-beta6
projects[entity_translation][patch][] = "https://drupal.org/files/issues/entity_translation-2218133-13.patch"
projects[extlink] = 1.18
projects[facetapi] = 7.x-1.5
projects[facetapi][patch][] = "https://www.drupal.org/files/issues/2076187-block-hash-deltas-facet-names-5.patch"
projects[facetapi][patch][] = "https://www.drupal.org/files/issues/2311585-3-facetapi-7.x-2.x-translate_more_link.patch"
projects[facetapi_collapsible] = 1.1
projects[facetapi_i18n] = 1.0-beta2
projects[facetapi_multiselect] = 1.0-beta1
projects[features] = 2.10
projects[features_extra] = 1.0
projects[features_override] = 2.0-rc3
projects[features_roles_permissions] = 1.2
projects[feeds] = 2.0-beta3
projects[feeds_tamper] = 1.1
projects[field_group] = 1.5
projects[file_entity] = 2.3
projects[filefield_paths] = 1.0
projects[fpa] = 2.6
projects[globalredirect] = 1.5
projects[google_analytics] = 2.3
projects[google_appliance] = 1.14
projects[google_appliance_suggest] = 1.6
projects[habitat] = 1.0
projects[honeypot] = 1.22
projects[i18n] = 1.18
projects[i18n_book_navigation] = 2.8
projects[i18nviews] = 3.0-alpha1
projects[imagecache_actions] = 1.7
projects[imagefield_focus] = 7.x-1.0
projects[imagefield_focus][patch][] = "https://drupal.org/files/issues/imagefield_focus-file-entity-integration-2521846-2.patch"
projects[insert] = 1.3
projects[job_scheduler] = 2.0-alpha3
projects[jquery_update] = 2.7
projects[l10n_update] = 2.1
projects[language_switcher] = 1.0-beta2
projects[ldap] = 7.x-2.2
projects[ldap][patch][] = "https://drupal.org/files/issues/ldap_pear_escape_filter_value-is_null.patch"
projects[libraries] = 2.3
projects[link] = 7.x-1.4
projects[link][patch][] = "https://www.drupal.org/files/issues/broken_parameter-2333119-16.patch"
projects[linkchecker] = 1.3
projects[linkit] = 3.5
projects[maxlength] = 3.2
projects[media] = 2.9
projects[media_ckeditor] = 2.1
projects[media_youtube] = 3.4
projects[menu_attributes] = 1.0
projects[menu_block] = 2.7
projects[menu_firstchild] = 1.1
projects[metatag] = 1.22
projects[migrate] = 7.x-2.8
projects[migrate][patch][] = "https://www.drupal.org/files/issues/migrate-invalid_argument_foreach-2644762-1.patch"
projects[migrate_d2d] = 2.1
projects[module_filter] = 2.1
projects[multiblock] = 1.5
projects[multifield] = 7.x-1.0-alpha4
projects[multifield][patch][] = "https://drupal.org/files/issues/multifield-features-integration-fix-api-2102265-32.patch"
projects[nice_menus] = 2.5
projects[og] = 2.9
projects[og_menu] = 3.1
projects[override_node_options] = 1.13
projects[paranoia] = 1.7
projects[pathauto] = 1.3
projects[pathauto_persist] = 1.4
projects[path_breadcrumbs] = 3.3
projects[pathologic] = 3.1
projects[path_redirect_import] = 1.0-rc4
projects[rabbit_hole] = 2.24
projects[redirect] = 1.0-rc3
projects[rules] = 2.10
projects[search_api] = 1.22
projects[search_api_db] = 1.6
projects[search_api_et] = 2.0-alpha7
projects[search_api_et_db] = 1.x-dev
projects[seckit] = 1.9
projects[security_review] = 1.2
projects[smtp] = 1.7
projects[strongarm] = 2.0
projects[taxonomy_csv] = 5.10
projects[telephone] = 1.0-alpha1
projects[term_reference_tree] = 1.10
projects[title] = 1.0-alpha9
projects[toc_filter] = 1.3
projects[token] = 1.7
projects[transliteration] = 7.x-3.2
projects[transliteration][patch][] = "https://www.drupal.org/files/transliteration-field_formatter-1925980-2.patch"
projects[uuid] = 1.0
projects[uuid_features] = 1.0-rc1
projects[variable] = 2.5
projects[viewfield] = 2.1
projects[video_embed_field] = 2.0-beta1
projects[view_unpublished] = 1.2
projects[views] = 7.x-3.16
projects[views][patch][] = "https://drupal.org/files/fixed_image_warnings-1959558-1.patch"
projects[views][patch][] = "https://www.drupal.org/files/issues/views-invalid_argument-2866370-14.patch"
projects[views_bulk_operations] = 3.4
projects[views_data_export] = 3.2
projects[views_field_view] = 7.x-1.2
projects[views_field_view][patch][] = "https://drupal.org/files/issues/views_field_view-null-value-of-last-rendered-2728619-2.patch"
projects[views_load_more] = 1.5
projects[views_slideshow] = 3.9
projects[webform] = 4.15
projects[webform_block] = 1.2
projects[webform_clear] = 7.x-2.0
projects[webform_clear][patch][] = "https://www.drupal.org/files/issues/2471671-test-withpatch.patch"
projects[webform_features] = 3.0-beta3
projects[webform_reply_to] = 2.0
projects[webform_steps] = 2.1
projects[webform_translation] = 7.x-1.0
projects[webform_translation][patch][] = "https://www.drupal.org/files/issues/webform_translation-2892216-1.patch"
projects[webform_validation = 1.13
projects[workbench = 1.2
projects[workbench_moderation] = 7.x-3.0
projects[workbench_moderation][patch][] = "https://drupal.org/files/issues/workbench_moderation-2218133-33.patch"
projects[workbench_moderation][patch][] = "https://drupal.org/files/issues/workbench_moderation-state-feeds-mapper-1470528-12.patch"


;LIBRARIES
;---------
libraries[jquerycycle][download][type]= "get"
libraries[jquerycycle][download][url] = "http://malsup.github.com/jquery.cycle.all.js"
libraries[jquerycycle][directory_name] = "jquery.cycle"
libraries[jquerycycle][destination] = "libraries"

libraries[ckeditor][download][type]= get"
libraries[ckeditor][download][url] = "http://download.cksource.com/CKEditor/CKEditor/CKEditor%204.4.4/ckeditor_4.4.4_standard.zip"
libraries[ckeditor][directory_name] = "ckeditor"
libraries[ckeditor][destination] = "libraries"

libraries[colorbox][download][type] = "git"
libraries[colorbox][download][url] = "http://github.com/jackmoore/colorbox.git"
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][destination] = "libraries"

libraries[chosen][download][type] = "git"
libraries[chosen][download][url] = "http://github.com/harvesthq/chosen.git"
libraries[chosen][directory_name] = "chosen"
libraries[chosen][destincation] = "libraries"

libraries[jquerycycle2][download][type] = "get"
libraries[jquerycycle2][download][url] = "http://jquery.malsup.com/cycle2/download/"
libraries[jquerycycle2][directory_name] = "jquery.cycle2"
libraries[jquerycycle2][destination] = "libraries"

libraries[json2][download][type] = "git"
libraries[json2][download][url] = "http://github.com/douglascrockford/json-js.git"
libraries[json2][directory_name] = "json2"
libraries[json2][destination] = "libraries"

;FEATURES
;--------
projects[gnwt-core][downloads][type] = "git"
projects[gnwt-core][download][url] = "git@bitbucket.org:gnwt-cue/gnwt-core.git"
projects[gnwt-core][directory_name] = "gnwt-core"
projects[gnwt-core][type] = "module"
projects[gnwt-core][subdir] = "features"

projects[gnwt-features-and-sliders][downloads][type] = "git"
projects[gnwt-features-and-sliders][download][url] = "git@bitbucket.org:gnwt-cue/gnwt-features-and-sliders.git"
projects[gnwt-features-and-sliders][directory_name] = "gnwt-features-and-sliders"
projects[gnwt-features-and-sliders][type] = "module"
projects[gnwt-features-and-sliders][subdir] = "features"

projects[gnwt-basic-page][downloads][type] = "git"
projects[gnwt-basic-page][download][url] = "git@bitbucket.org:gnwt-cue/gnwt-basic-page.git"
projects[gnwt-basic-page][directory_name] = "gnwt-basic-page"
projects[gnwt-basic-page][type] = "module"
projects[gnwt-basic-page][subdir] = "features"

projects[gnwt-resources-package][downloads][type] = "git"
projects[gnwt-resources-package][download][url] = "git@bitbucket.org:gnwt-cue/gnwt-resources-package.git"
projects[gnwt-resources-package][directory_name] = "gnwt-resources-package"
projects[gnwt-resources-package][type] = "module"
projects[gnwt-resources-package][subdir] = "features"

projects[gnwt-news-package][downloads][type] = "git"
projects[gnwt-news-package][download][url] = "git@bitbucket.org:gnwt-cue/gnwt-news-package.git"
projects[gnwt-news-package][directory_name] = "gnwt-news-package"
projects[gnwt-news-package][type] = "module"
projects[gnwt-news-package][subdir] = "features"

projects[gnwt-services][downloads][type] = "git"
projects[gnwt-services][download][url] = "git@bitbucket.org:gnwt-cue/gnwt-services.git"
projects[gnwt-services][directory_name] = "gnwt-services"
projects[gnwt-services][type] = "module"
projects[gnwt-services][subdir] = "features"

projects[gnwt-multilingual-package][downloads][type] = "git"
projects[gnwt-multilingual-package][download][url] = "git@bitbucket.org:gnwt-cue/gnwt-multilingual-package.git"
projects[gnwt-multilingual-package][directory_name] = "gnwt-multilingual-package"
projects[gnwt-multilingual-package][type] = "module"
projects[gnwt-multilingual-package][subdir] = "features"
